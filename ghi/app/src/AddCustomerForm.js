import React, { useState, useEffect } from 'react';


function AddCustomerForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        phone_number: '',
        address: '',
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleFormSubmit = async function(event) {
        event.preventDefault();
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        });
        const data = await response.json();
        console.log(data);
        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                phone_number: '',
                address: '',
            });
            console.log("form submitted successfully");
        }
    };


    return (
        <div>
            <h1>Add Customer</h1>
            <form onSubmit={handleFormSubmit}>
                <div className="form-group">
                    <label htmlFor="first-name">First Name</label>
                    <input type="text" id="first-name" name="first_name" className="form-control" value={formData.first_name} onChange={handleFormChange} />
                </div>
                <div className="form-group">
                    <label htmlFor="last-name">Last Name</label>
                    <input type="text" id="last-name" name="last_name" className="form-control" value={formData.last_name} onChange={handleFormChange} />
                </div>
                <div className="form-group">
                    <label htmlFor="phone-number">Phone Number</label>
                    <input type="text" id="phone-number" name="phone_number" className="form-control" value={formData.phone_number} onChange={handleFormChange} />
                </div>
                <div className="form-group">
                    <label htmlFor="address">Address</label>
                    <input type="text" id="address" name="address" className="form-control" value={formData.address} onChange={handleFormChange} />
                </div>
                <button type="submit" className="btn btn-primary">Add Customer</button>
            </form>
        </div>
    );
}

export default AddCustomerForm;
