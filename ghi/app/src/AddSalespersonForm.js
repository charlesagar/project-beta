import React, { useState, useEffect } from 'react';


function AddSalespersonForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    });



    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleFormSubmit = async function(event) {
        event.preventDefault();
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        });
        const data = await response.json();
        console.log(data);
        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            });
            console.log("form submitted successfully");
        }
    };

    return (
        <div>
            <h1>Add Salesperson</h1>
            <form onSubmit={handleFormSubmit}>
                <div className="form-group">
                    <label htmlFor="first-name">First Name</label>
                    <input type="text" id="first-name" name="first_name" className="form-control" value={formData.first_name} onChange={handleFormChange} />
                </div>
                <div className="form-group">
                    <label htmlFor="last-name">Last Name</label>
                    <input type="text" id="last-name" name="last_name" className="form-control" value={formData.last_name} onChange={handleFormChange} />
                </div>
                <div className="form-group">
                    <label htmlFor="employee-id">Employee ID</label>
                    <input type="text" id="employee-id" name="employee_id" className="form-control" value={formData.employee_id} onChange={handleFormChange} />
                </div>
                <button type="submit" className="btn btn-primary">Add</button>
            </form>
        </div>
    )







}

export default AddSalespersonForm;
