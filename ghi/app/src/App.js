import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import AddTechnicianForm from './AddTechnicianForm';
import AddAppointmentForm from './AddAppointmentForm';
import AppointmentList from './AppointmentList';
import AppointmentsByVin from './AppointmentsByVin';
import VehicleModelsList from './VehicleModelsList';
import CreateAVehicleModel from './CreateAVehicleModel';
import ListManufacturers from './ListManufacturers';
import CreateManufacturer from './CreateManufacturer';
import CreateAutomobileForm from './CreateAutomobileForm';
import AutomobilesList from './AutomobilesList';
import SalespeopleList from './SalespeopleList';
import CustomersList from './CustomersList';
import AddCustomerForm from './AddCustomerForm';
import AddSalespersonForm from './AddSalespersonForm';
import CreateNewSaleForm from './CreateNewSaleForm';
import SalesList from './SalesList';
import SalespersonHistoryList from './SalespersonHistoryList';

function App() {
  const [refresh, setRefresh] = useState(false);

  const handleFormSubmit = () => {
    setRefresh(prevState => !prevState);
  };

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/add-technician" element={<AddTechnicianForm />} />
          <Route path="/add-appointment" element={<AddAppointmentForm />} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments-by-vin" element={<AppointmentsByVin />} />
          <Route path="/models" element={<VehicleModelsList />} />
          <Route path="/create-vehicle-model" element={<CreateAVehicleModel />} />
          <Route path="/manufacturers" element={<ListManufacturers key={refresh} />} />
          <Route path="/create-manufacturer" element={<CreateManufacturer onFormSubmit={handleFormSubmit} />} />
          <Route path="/create-automobile" element={<CreateAutomobileForm />} />
          <Route path="/automobiles-list" element={<AutomobilesList />} />
          <Route path="/salespeople" element={<SalespeopleList />} />
          <Route path="/customers" element={<CustomersList />} />
          <Route path="/add-customer" element={<AddCustomerForm />} />
          <Route path="/add-salesperson" element={<AddSalespersonForm />} />
          <Route path="/create-new-sale" element={<CreateNewSaleForm />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/salesperson-history" element={<SalespersonHistoryList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
