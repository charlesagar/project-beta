import React, { useState, useEffect } from 'react';
import CustomersItem from './CustomersItem';

const CustomersList = function() {
    const [customers, setCustomers] = useState([]);

    const getCustomers = async function() {
        const url = 'http://localhost:8090/api/customers';
        const response = await fetch(url);
        const data = await response.json();
        setCustomers(data.customers);
    }

    useEffect(() => {
        getCustomers();
    }, []);

    return (
        <>
            <h1>Customers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => <CustomersItem key={customer.id} customer={customer} />)}
                </tbody>
            </table>
        </>
    )
};

export default CustomersList;
