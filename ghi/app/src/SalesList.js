import React, { useState, useEffect } from 'react';
import SalesItem from './SalesItem';

const SalesList = function() {
    const [sales, setSales] = useState([]);

    const getSales = async function() {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        const data = await response.json();
        console.log('Sales data:', data);
        setSales(data);
    }

    useEffect(() => {
        getSales();
    }, []);

    return (
        <>
            <h1>Sales List</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>Vin</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales && sales.map(sale => <SalesItem key={sale.id} sale={sale} />)}
                </tbody>
            </table>
        </>
    )
}

export default SalesList;
