import React, { useState, useEffect } from 'react';
import SalespeopleItem from './SalespeopleItem';

const SalespeopleList = function() {
    const [salespeople, setSalespeople] = useState([]);

    const getSalespeople = async function() {
        const url = 'http://localhost:8090/api/salespeople';
        const response = await fetch(url);
        const data = await response.json();
        setSalespeople(data.salespeople);
    }

    useEffect(() => {
        getSalespeople();
    }, []);





    return (
        <>
            <h1>Sales Person List </h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => <SalespeopleItem key={salesperson.employee_id} salesperson={salesperson} />)}
                </tbody>
            </table>
        </>
    )
}

export default SalespeopleList;
