import React, { useState, useEffect } from 'react';
import SalespersonHistoryItem from './SalespersonHistoryItem';

const SalespersonHistoryList = function() {
    const [sales, setSales] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState(null);

    const getSalespeople = async function() {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        const data = await response.json();
        setSalespeople(data.salespeople);
    }

    const getSales = async function() {
        if (selectedSalesperson) {
            const url = `http://localhost:8090/api/sales/?salesperson=${selectedSalesperson.employee_id}`;
            console.log(`Fetching sales for salesperson ${selectedSalesperson.employee_id} from ${url}`);
            const response = await fetch(url);
            const data = await response.json();
            console.log('Received sales data:', data);
            setSales(data);
        }
    }

    useEffect(() => {
        getSalespeople();
    }, []);

    useEffect(() => {
        if (selectedSalesperson) {
            getSales();
        }
    }, [selectedSalesperson ? selectedSalesperson.employee_id : null]);

    return (
        <>
            <h1>Salesperson History</h1>
            <select onChange={e => setSelectedSalesperson(salespeople.find(s => s.employee_id === e.target.value))}>
                {salespeople.map(salesperson => <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name}</option>)}
            </select>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>Vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => <SalespersonHistoryItem key={sale.id} sale={sale} />)}
                </tbody>
            </table>
        </>
    )
}

export default SalespersonHistoryList;
