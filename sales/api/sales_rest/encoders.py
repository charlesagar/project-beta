from common.json import ModelEncoder
from decimal import Decimal
from .models import Sale, Salesperson, Customer, AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "sold",
    ]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "customer",
        "automobile",
        "salesperson",
    ]
    encoders = {
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
    }

    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)
        return super().default(obj)
