from django.db import models
from django.urls import reverse

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100,unique=True)
    sold = models.BooleanField(default=False)

class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=30)

    def get_api_url(self):
        return reverse("api_list_salespeople", kwargs={"pk": self.pk})

class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=100, default="N/A")
    phone_number = models.CharField(max_length=100, default="N/A")

    def get_api_url(self):
        return reverse("api_list_customers", kwargs={"pk": self.pk})

class Sale(models.Model):
    price = models.DecimalField(max_digits=100, decimal_places=2)

    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE)

    automobile = models.ForeignKey(
        AutomobileVO,
        on_delete=models.CASCADE)

    salesperson = models.ForeignKey(
        Salesperson,
        on_delete=models.CASCADE)

    def get_api_url(self):
        return reverse("api_list_sales", kwargs={"pk": self.pk})
