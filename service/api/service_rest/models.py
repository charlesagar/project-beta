from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse
# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField(default=False)

class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField(primary_key=True)

    def get_api_url(self):
        return reverse("show_technician", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    class Meta:
        ordering = ("first_name", "employee_id")

class Appointment(models.Model):
    date_time = models.DateTimeField(auto_now_add=True)
    reason = models.CharField(max_length=100)
    status = models.CharField(max_length=50)
    vin = models.CharField(max_length=200, default='default_vin')
    customer = models.CharField(max_length=50)
    technician = models.ForeignKey(
        Technician,
        related_name="+",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("show_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return f"Appointment {self.id} - {self.reason} at {self.date_time}"

    class Meta:
        ordering = ("date_time", "reason")
