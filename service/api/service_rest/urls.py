from django.urls import path
from .views import api_auto_list, api_technician_list, api_appointment_list, api_appointment_detail

urlpatterns = [
    path('auto/', api_auto_list, name="api_auto_list"),
    path('technicians/', api_technician_list, name="api_technician_list"),
    path('appointments/', api_appointment_list, name="api_appointment_list"),
    path('appointments/<int:id>/', api_appointment_detail, name='api_appointment_detail'),
]
