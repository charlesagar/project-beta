

from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment
from django.http import JsonResponse
import json
from django.core.exceptions import ValidationError
from django.views.decorators.csrf import csrf_exempt

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]
class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id"
    ]
class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "status",
        "customer",
        "technician",
        "vin",
        "reason",
        "date_time"
    ]
    encoders = {
        "technician": TechnicianEncoder()
    }
def api_auto_list(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            {"autos": autos},
            encoder=AutomobileVOEncoder,
        )
def api_technician_list(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
        {"technicians": technicians},
        encoder=TechnicianEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
    )
def api_appointment_list(request):
    if request.method == "GET":
        vin = request.GET.get('vin', None)
        if vin is not None:
            appointments = Appointment.objects.filter(vin=vin)
        else:
            appointments = Appointment.objects.all()
        data = [AppointmentListEncoder().default(appointment) for appointment in appointments]
        return JsonResponse({"appointments": data}, safe=False, status=200)
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(employee_id=content.pop('employee_id'))
            content['technician'] = technician
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                AppointmentListEncoder().default(appointment),
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse({"error": "Technician with provided employee_id does not exist"}, status=400)
        except ValidationError as e:
            return JsonResponse({"error": str(e)}, status=400)
    elif request.method == "DELETE":
        content = json.loads(request.body)
        try:
            appointment = Appointment.objects.get(id=content['id'])
            appointment.delete()
            return JsonResponse({"status": "Appointment deleted"}, status=200)
        except Appointment.DoesNotExist:
            return JsonResponse({"error": "Appointment with provided id does not exist"}, status=400)

@csrf_exempt
def api_appointment_detail(request, id):
    if request.method == 'DELETE':
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse({"status": "Appointment deleted"}, status=200)
        except Appointment.DoesNotExist:
            return JsonResponse({"error": "Appointment with provided id does not exist"}, status=400)
    elif request.method == 'PUT':
        try:
            data = json.loads(request.body)
            appointment = Appointment.objects.get(id=id)
            appointment.status = data.get('status')
            appointment.save()
            return JsonResponse(AppointmentListEncoder().default(appointment), safe=False, status=200)
        except Appointment.DoesNotExist:
            return JsonResponse({"error": "Appointment with provided id does not exist"}, status=400)
        except ValueError:
            return JsonResponse({"error": "Invalid JSON data"}, status=400)
