import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

from service_rest.models import AutomobileVO #finds service_rest!

def get_automobiles():
    response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles/")
    content = json.loads(response.content)
    for auto in content["autos"]:
        print("auto being saved: ", auto)
        AutomobileVO.objects.update_or_create(
            vin=auto["vin"],
            defaults={
                "sold":auto["sold"]
            }
        )
# Import models from service_rest, here. Ignore vs-code error hinting
# from service_rest.models import Something

#"project-beta-inventory-api-1" host name for url

def poll():
    while True:
        print('Service poller polling for data')
        try:
            # Write your polling logic, here
            # Do not copy entire file
            get_automobiles()

            pass

        except Exception as e:
            print(e, file=sys.stderr)

        time.sleep(60)


if __name__ == "__main__":
    poll()
